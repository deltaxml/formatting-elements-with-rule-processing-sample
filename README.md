# Formatting Elements With Rule Processing Sample

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the XML Merge release.*    
*The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-XML-Merge-7_0_0_j/samples/sample-name`.*    

------   

When the rule processing is enabled, XML Merge accepts simple adds, deletes and modification. All of these rules are applied to attributes, elements and their contents.     
However, when it comes to formatting elements, the rule processing is applied to formatting element tags instead of their contents. The formatting elements tags are     
usually added or deleted around the content in the inputs.   
    
For full information about the formatting elements sample see our web page [Formatting Elements With Rule Processing Sample](https://docs.deltaxml.com/xml-merge/latest/samples-and-guides/formatting-elements-with-rule-processing-sample)     
    
To run this sample, you will require [Apache Ant](http://ant.apache.org/) and a [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html). From a command prompt in the sample directory, you can compile and run the sample with:

     ant    
      
The following targets are supplied in build.xml:

| Target | Description |
| ------- |  --------- |
| run | Default. This depends on the compile target, and runs the main method of [FormattingElementsWithRuleProcessing.java](https://bitbucket.org/deltaxml/formatting-elements-with-rule-processing-sample/src/default/FormattingElementsWithRuleProcessing.java). |
| compile | Compiles FormattingElementsWithRuleProcessing.java into a Java .class file in the class directory, which can be invoked either through the java command or from the ant run target above. |
| clean | This deletes everything in the .class directory. |

