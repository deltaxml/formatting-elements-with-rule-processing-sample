// Copyright (c) 2024 Deltaman group limited. All rights reserved.

import com.deltaxml.cores9api.FilterChain;
import com.deltaxml.cores9api.FilterStepHelper;
import com.deltaxml.merge.ConcurrentMerge;
import com.deltaxml.merge.ConcurrentMerge.ExtensionPoint;
import com.deltaxml.merge.ConcurrentMerge.MergeResultType;
import com.deltaxml.merge.RuleConfiguration;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Set;
import java.util.TreeSet;

public class FormattingElementsWithRuleProcessing {
  public static void main(String[] args) throws Exception {
    File samplesResultFolder = new File("results/");

    // Ancestor (A) and Version (B,C,D) files
    File aVersion = new File("FEInput1-A.xml");
    File bVersion = new File("FEInput1-B.xml");
    File cVersion = new File("FEInput1-C.xml");

    // Result File
    File resultFile = new File("result.xml");
    
    System.out.println("Running Concurrent Merge....");
    ConcurrentMerge merge= new ConcurrentMerge();
    
    /* 
      Specify which elements to consider as formatting:
      Use the mark-formatting.xsl stylesheet in the
      PRE_FLATTENING extension point to
      add deltaxml:format='true' attributes to thos elements.
    */
    FilterStepHelper fsh;
    fsh= merge.getFilterStepHelper();
    FilterChain fc;
    fc= fsh.newSingleStepFilterChain(new File("mark-formatting.xsl"), "mark-formatting");
    merge.setExtensionPoint(ExtensionPoint.PRE_FLATTENING, fc);
     
    merge.setAncestor(aVersion, "A");
    System.out.println("Ancestor (\"A\") set");
    
    merge.addVersion(bVersion, "B");
    System.out.println("Version \"B\" added");
    
    merge.addVersion(cVersion, "C");
    System.out.println("Version \"C\" added");
    
    System.out.println("Extracting DeltaV2.1 format output without rule processing");
    merge.extractAll(new File(samplesResultFolder, "deltav21.xml"));
    
    System.out.println("Extracting DeltaV2.1 format output with rule processing");
    merge.setResultType(MergeResultType.RULE_PROCESSED_DELTAV2);
    merge.extractAll(new File(samplesResultFolder, "deltav21-rule-processed.xml"));
    
    System.out.println("Extracting DeltaV2.1 format output with rule processing - Display Changes Involving Version B");
    RuleConfiguration rc= new RuleConfiguration();
    Set<String> involving= new TreeSet<>();
    involving.add("B");
    rc.setDisplayChangesInvolving(involving);
    merge.setRuleConfiguration(rc);
    merge.extractAll(new File(samplesResultFolder, "deltav21-rule-processed-changes-involving-B.xml"));
    
    System.out.println("Extracting DeltaV2.1 format output with rule processing - Display Formatting Changes In all 'para' eleemnts");
    RuleConfiguration rc1= new RuleConfiguration();
    rc1.setDisplayFormatChangesIn("//para");
    merge.setRuleConfiguration(rc1);
    merge.extractAll(new File(samplesResultFolder, "deltav21-rule-processed-formatting-changes-changes-in.xml"));

    System.out.println("Result files created at: " + samplesResultFolder.getCanonicalPath());
    System.out.println("");
    
  }
}
